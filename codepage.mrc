
;Right click on a channel/status/query and select "Add a codepage for this network"

on *:start:if ($starting) && ($isfile(codepage)) hload -m codepage codepage
menu channel,status,query {
  $iif($network,Codepage Support)
  .$iif(!$hget(codepage,$network),Add a codepage for this network) : if ($$input(Select a codepage,m,Codepage selection, [ $codepage_selection  ] )) && ($regex($charset,$v1 $+ =(\S+))) hadd -m codepage $network $regml(1) | hsave codepage codepage
  .$iif($hget(codepage,$network),Remove a codepage for this network) : hdel codepage $network | if (!$hget(codepage,0).item) .remove codepage | else hsave codepage codepage
}
on *:PARSELINE:out:*:if ($hget(codepage,$network)) parseline -ot $utftocodepage($parseline,$v1)
alias utftocodepage return $utfdecode($utfencode($1),$2)
alias codepage_selection return $regsubex($charset,/(^\S+)?=([^ ]+)(?: |$)/gF,$+(\1,$chr(44),\1,$chr(44)))
alias charset return ANSI=0 DEFAULT=1 SYMBOL=2 MAC=77 SHIFTJIS=128 HANGUL=129 JOHAB=130 GB2312=134 CHINESEBIG5=136 GREEK=161 TURKISH=162 VIETNAMESE=163 HEBREW=177 ARABIC=178 RUSSIAN=204 BALTIC=186 THAI=222 EASTEUROPE=238 OEM=255
